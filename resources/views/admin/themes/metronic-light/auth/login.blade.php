@extends('admin::auth.layout')

@section('content')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@700&family=Merriweather:wght@700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-mQTe3hnpKcVlUzwnKLG4gXe1ucKwUt9IxjF7Vws3eLP/0lOwP+Esq3FXGe8MjIeboMI+kFXQDGlCEar8I0Fvng==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
        .kt-grid__item.kt-grid__item--fluid.kt-grid.kt-grid--hor {
        background-image: none !important;
    }
    body {
        background: white;
    }

    input.form-control {
        border-radius: 3px !important;
        border: .5px solid rgba(67,34,167,.4) !important;
        background: none !important;
    }
    button#kt_login_signin_submit {
        background: rgb(161, 130, 69);
        border-radius: 0px !important;
        width:84%;
        margin-left:70px;
        
    }

    body {
        margin: 0;
    }

    .logo-img {
        position: fixed;
        top: 25px;
        left: 25px;
        margin: 10px; 
    }
    .kt-login__head p{
        text-align: center;

    }
    .input-group.row {
        display: flex;
        align-items: baseline;
        gap: 0px;
    }
    /* .input-group.row label {
        flex: 0 0 100px; 
    }

    .input-group.row input {
        flex: 1; 
    } */
    h3.kt-login__title {
        font-weight: 500 !important;
        font-size: 30px !important;
        font-style: normal !important;
        color: rgb(0, 0, 0) !important;
        text-decoration: none !important;
    }
    .row.kt-login__extra {
    padding-left: 60px !important;
    margin-bottom:0px !important;
}
.kt-login__actions {
    margin-top: 0px !important;
}
.invalid-feedback {
    margin-left: 70px !important;
}
h3.kt-login__title {
    margin-left: 42px;
}
p {
    margin-left: 42px;
}
    </style>

    @if (session('status'))
      <div class="alert alert-success alert-dismissible upper" id="flash-msg">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          {{ session('status') }}
      </div>
    @endif
    <div class="logo-img">
        <img src="https://dev-bloodhound.s3.amazonaws.com/BHI.CMYK.Reg.jpg" alt="" width="200">
    </div>

    <div class="kt-login__signin">
    
        <div class="kt-login__head">    
            <h3 class="kt-login__title"><span style="font-style:normal; font-weight:700; font-family: 'Fira Sans', sans-serif;">Log</span> in</h3>
            <p> Bloodhound </p>
        </div>
        <form class="kt-form" method="post" action="{{ route('login') }}" style="margin:0px">
            {{ csrf_field() }}
            <div class="input-group row">
                <label style="margin-left: 28px; margin-right:10px">Email</label>
                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                @if ($errors->has('email'))
                    <div class="invalid-feedback" style="display: block;">{{ $errors->first('email') }}</div>
                @endif
            </div>
            <div class="input-group row">
                <label style="margin-right:10px">Password</label>
                <input class="form-control" type="password" placeholder="Password" name="password">
                @if ($errors->has('password'))
                    <div class="invalid-feedback" style="display: block;">{{ $errors->first('password') }}</div>
                @endif
            </div>
            <div class="row kt-login__extra">
                <div class="col">
                    <input type="checkbox" name="remember"> 
                    <label>
                        <span style="color: black;">Remember me</span>
                    </label>
                </div>
                <a href="{{ route('forget.password') }}" class="forgot-password-link">Forgot Password ?</a> 
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary btn-large"><strong>Log in</strong>  <i class="fa fa-thin fa-arrow-right"></i></button>
            </div>
        </form>
    </div>

    @if(config('admin.auth.register'))
        <div class="kt-login__signup">
            <div class="kt-login__head">
                <h3 class="kt-login__title">Sign Up</h3>
                <div class="kt-login__desc">Enter your details to create your account:</div>
            </div>
            <form class="kt-login__form kt-form" method="post" action="{{ route('register') }}">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Fullname" name="name" autocomplete="off">
                    @if ($errors->has('name'))
                        <div class="invalid-feedback" style="display: block;">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                    @if ($errors->has('email'))
                        <div class="invalid-feedback" style="display: block;">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="input-group">
                    <input class="form-control" type="password" placeholder="Password" name="password">
                    @if ($errors->has('password'))
                        <div class="invalid-feedback" style="display: block;">{{ $errors->first('password') }}</div>
                    @endif
                </div>
                <div class="input-group">
                    <input class="form-control" type="password" placeholder="Confirm Password" name="password_confirmation">
                    @if ($errors->has('password_confirmation'))
                        <div class="invalid-feedback" style="display: block;">{{ $errors->first('password_confirmation') }}</div>
                    @endif
                </div>
                @if(config('admin.auth.terms_and_conditions_route'))
                    <div class="row kt-login__extra">
                        <div class="col kt-align-left">
                            <label class="kt-checkbox" for="agree">
                                I Agree the <a href="{{ route(config('admin.auth.terms_and_conditions_route')) }}" class="kt-link kt-login__link kt-font-bold">terms and conditions</a>.
                                <input type="checkbox" id="agree" name="agree" />
                                <span></span>
                            </label>
                            <span class="form-text text-muted"></span>
                        </div>
                        @if ($errors->has('agree'))
                            <div class="invalid-feedback" style="display: block;">{{ $errors->first('agree') }}</div>
                        @endif
                    </div>
                @endif
                <div class="kt-login__actions">
                    <button id="kt_login_signup_submit" class="btn btn-pill kt-login__btn-primary">Sign Up</button>&nbsp;&nbsp;
                    <button id="kt_login_signup_cancel" class="btn btn-pill kt-login__btn-secondary">Cancel</button>
                </div>
            </form>
        </div>
    @endif

    @if(config('admin.auth.forgot_password'))
        <div class="kt-login__forgot">
            <div class="kt-login__head">
                <h3 class="kt-login__title">Forgotten Password ?</h3>
                <div class="kt-login__desc">Enter your email to reset your password:</div>
            </div>
            <form class="kt-form" method="post" action="{{ route('password.email') }}">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
                    @if ($errors->has('email'))
                        <div class="invalid-feedback" style="display: block;">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="kt-login__actions">
                    <button id="kt_login_forgot_submit" class="btn btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
                    <button id="kt_login_forgot_cancel" class="btn btn-pill kt-login__btn-secondary">Cancel</button>
                </div>
            </form>
        </div>
    @endif

    @if(config('admin.auth.register'))
        <div class="kt-login__account">
            <span class="kt-login__account-msg">
                Don't have an account yet ?
            </span>&nbsp;&nbsp;
            <a href="javascript:;" id="kt_login_signup" class="kt-link kt-link--light kt-login__account-link">Sign Up</a>
        </div>
    @endif

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#flash-msg").delay(3000).fadeOut("slow");
        });
    </script>
@endsection
